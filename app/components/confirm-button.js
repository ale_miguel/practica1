import Component from '@ember/component';

export default Component.extend({
	isConfirming: false,
	actions: {
		triggerAction(text){
			this.set('isConfirming', true)
			if(confirm("¿Estás seguro de " + text + "?")){
				Promise.resolve(this.onCall())
					.then(()=>{
						console.log('ya acabaste')
						this.set('isConfirming', false)
					})
					.catch(()=>{

					})
			}
		}
	}
});
