import Component from '@ember/component';

export default Component.extend({
	actions:{
		editProduct(){
			this.onEditProduct()
		},
		deleteProduct(){
			this.onDeleteProduct()
		}
	}
});
