import Component from '@ember/component';

export default Component.extend({
	tagName:'nav', //un componente de etiqueta, en vez que por default ponga un div, va a poner lo que se le ponga
	classNames: 'primary',
	classNameBindings:['isOld:old'],
	isOld: false  
});
/*export default Component.extend({
}).reopenClass({
	positionalParams:['title', 'name'] //Para pasar parámetros 
});
*/