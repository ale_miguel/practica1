import Controller from '@ember/controller';

export default Controller.extend({
	cat: 'pelusa',
	weekDays: [{
			day: 'monday',
			number: 1,
			'mi propiedad': 'hola'
		},
		{
			day: 'tuesday',
			number: 2,
			'mi propiedad': 'hola'
		},
		{
			day: 'wednesday',
			number: 3,
			'mi propiedad': 'hola'
		},
		{
			day: 'thursday',
			number: 4,
			'mi propiedad': 'hola'
		},
		{
			day: 'friday',
			number: 5,
			'mi propiedad': 'hola'
		},
		{
			day: 'saturday',
			number: 6,
			'mi propiedad': 'hola'
		},
		{
			day: 'sunday',
			number: 7,
			'mi propiedad': 'hola'
		},
	],
	tweets: [],
	isSaving: false,
	actions: {
		doSomething() {
			this.set('cat', "perro");
			let cat = this.get('cat');

			//console.log(`The name is ${cat}`)

			alert('ALERT');
		},
		myLuckyDay() {
			let weekDays = this.get('weekDays')

			var n = Math.floor(Math.random() * weekDays.length);
			this.set('luckyDay', weekDays.objectAt(n)['day']);
			//this.set('luckyDay', weekDays.objectAt(n).day)
		},
		addDay() {
			let newDay = this.get('newDay');
			let weekDays = this.get('weekDays')
			weekDays.pushObject({
				day: newDay,
				//number: Math.floor(Math.random()*100),
				number: weekDays.length + 1,
				'mi propiedad': 'hola'
			});
		},
		addTweet() {
			let newTweet = this.get('newTweet');
			let tweets = this.get('tweets');

			tweets.pushObject({
				user: 'Ale_Miguel',
				description: newTweet,
			});

			this.set('newTweet', '');

		},
		logout() {
			this.transitionToRoute('login');
		},
		deleteTweet(tweet) {

			let result = confirm('deseas borrar este tweet : ' + tweet.description)

			if (result) {
				this.get('tweets').removeObject(tweet);
			}
		},

		sendMeal() {
			var resultado = confirm("¿Estás seguro que quieres añadir la comida?");

			if (resultado) {
				console.log("Verificando")
				this.set('isSaving', true);

				this.store.createRecord('meal', {
					name: this.get('meal')
				}).save().then(() => {
					console.log("Si funciona")

				}).catch((e) => {
					alert("Uy no, esto se rompió :'v")

				}).finally(() => {
					this.set('isSaving', false);
				});
			}

		},

		editMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres editar la comida?");
			if (resultado) {
				meal.save();
			}

		},

		deleteMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres eliminar la comida?");
			if (resultado) {
				meal.destroyRecord();
			}

		}
	}
});
