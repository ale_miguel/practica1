import Controller from '@ember/controller';

export default Controller.extend({
	actions: {

		sendMeal() {
			var resultado = confirm("¿Estás seguro que quieres añadir la comida?");

			if (resultado) {
				console.log("Verificando")
				this.set('isSaving', true);

				this.store.createRecord('meal', {
					name: this.get('meal')
				}).save().then(() => {
					console.log("Si funciona")

				}).catch((e) => {
					alert("Uy no, esto se rompió :'v")

				}).finally(() => {
					this.set('isSaving', false);
				});
			}

		},

		editMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres editar la comida?");
			if (resultado) {
				meal.save();
			}

		},

		deleteMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres eliminar la comida?");
			if (resultado) {
				meal.destroyRecord();
			}

		},

		uselessAction(){
			alert('just an alert!')
		}
	}
});
