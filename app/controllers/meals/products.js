import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		createProduct(){
			this.store.createRecord('product', {
				name: this.get('name'),
				calories: this.get('calories')
			}).save().then(()=>{
				this.set('name', '')
				this.set('calories', '')
				alert('Producto guardado.')
			})
		},
		deleteProduct(product){
			product.destroyRecord().then(()=>{
				alert("Producto eliminado.")
			})
		},
		updateProduct(product){
			product.save().then(()=>{
				alert("Producto actualizado.")
			})
		}
	}
});
