import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

Router.map(function() {
    this.route('lab', { path: '/lab' });
    this.route('login', { path: '/' }); //Para hacer que sea lo primero que se muestre "path raíz"
    this.route('carrot');

    this.route('meals', function() {
      this.route('meal-detail', { path: '/:id' });
      this.route('products');
    });
});

export default Router;
